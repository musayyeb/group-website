from app import app
from flask import render_template


@app.route('/')
@app.route('/index')
def index():
    return "Hello, World"


@app.route('/about_us')
def about_us():
    return render_template('aboutus.html')
